﻿using UnityEngine;
using System.Collections;

public class RepeatSpawner : MonoBehaviour
{

    public GameObject[] prefabs;
    public float minTime = .5f;
    public float maxTime = 2f;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(SpawnRoutine());
    }

    IEnumerator SpawnRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(minTime, maxTime));
            Instantiate(prefabs[Random.Range(0, prefabs.Length)], transform.position, transform.rotation);
        }
    }
}
