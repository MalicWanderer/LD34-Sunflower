﻿using UnityEngine;
using System.Collections;

public class MoveUp : MonoBehaviour
{
    public float speed = 3;

    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rb == null)
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        }
        else
        {
            rb.MovePosition(transform.position + transform.up * speed * Time.deltaTime);
        }
    }
}
