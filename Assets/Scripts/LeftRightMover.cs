﻿using UnityEngine;
using System.Collections;

public class LeftRightMover : MonoBehaviour
{
    public float speed = 5;
    public string axisName = "Horizontal";
    public float margin = .5f;

    // Update is called once per frame
    void Update()
    {
        var move = Input.GetAxis(axisName) * speed * Time.deltaTime;

        var rightEdge = Camera.main.aspect * Camera.main.orthographicSize - margin;
        var leftEdge = -rightEdge;

        var pos = transform.position;
        pos.x = Mathf.Clamp(pos.x + move, leftEdge, rightEdge);
        transform.position = pos;

    }
}
