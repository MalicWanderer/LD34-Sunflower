﻿using UnityEngine;
using System.Collections;

public class LookUpAtTarget : MonoBehaviour
{
    public Transform target;
    public float speed = 10;

    // Update is called once per frame
    void Update()
    {
        var angle = Vector2.Angle(transform.up, target.position - transform.position);
        if (target.position.x > transform.position.x) angle *= -1;

        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.AngleAxis(angle, Vector3.forward), speed * Time.deltaTime);
    }
}
