﻿using UnityEngine;
using System.Collections;

public class AnimationTriggerer : MonoBehaviour
{
    public Animator target;
    public string triggerName;

    public void TriggerNow()
    {
        target.SetTrigger(triggerName);
    }
}
