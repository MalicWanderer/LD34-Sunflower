﻿using UnityEngine;
using System.Collections;

public class CameraBackgroundGradient : MonoBehaviour
{

    public Gradient gradient;
    public float endTime = 60;

    public Behaviour enableWhenDone;

    private Camera cam;

    // Use this for initialization
    void Start()
    {
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        cam.backgroundColor = gradient.Evaluate(Time.timeSinceLevelLoad / endTime);

        if (Time.timeSinceLevelLoad > endTime)
        {
            if (enableWhenDone != null) enableWhenDone.enabled = true;
            Destroy(this);
        }
    }
}
