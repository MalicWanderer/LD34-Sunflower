﻿using UnityEngine;
using System.Collections;

public class MoveAtStart : MonoBehaviour
{

    public Vector3 target;

    // Use this for initialization
    void Start()
    {
        transform.position = target;
    }
}
